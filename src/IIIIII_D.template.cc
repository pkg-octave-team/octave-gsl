

// PKG_ADD: autoload ("GSL_OCTAVE_NAME", which ("gsl_sf"));
DEFUN_DLD(GSL_OCTAVE_NAME, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} GSL_OCTAVE_NAME (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} GSL_OCTAVE_NAME (@dots{})\n\
\n\
GSL_FUNC_DOCSTRING
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
DEPRECATION_WARNING
@end deftypefn\n")
{
#ifdef HAVE_GSL_FUNC

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 6;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          GSL_FUNC_NAME (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          GSL_FUNC_NAME (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_FUNC undefined

  error ("GSL function GSL_FUNC_NAME was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_FUNC
}
