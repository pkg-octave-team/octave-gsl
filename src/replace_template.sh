#!/bin/bash

SED=/bin/sed

capfuncname=`echo ${funcname}|tr a-z A-Z`

${SED} -e 's/\\/\\\\/g;s/$/\\n\\/g' \
       < docstring.txt > docstring2.txt

DEPREC_INFO=`grep ${funcname} DEPRECATED`
if [ -z "$DEPREC_INFO" ]; then
    DEPREC_CMD="/DEPRECATION_WARNING/d"
else
    DEPREC_VER=${DEPREC_INFO##* }
    DEPREC_CMD="s/DEPRECATION_WARNING/\\\\n\\\\\\n@strong{DEPRECATION WARNING}: ${funcname} has been deprecated in GSL ${DEPREC_VER}\\\\n\\\\\\n\\\\n\\\\/"
fi

${SED} -e "s/GSL_OCTAVE_NAME/$octave_name/g" \
       -e "s/GSL_FUNC_NAME/$funcname/g" \
       -e "s/HAVE_GSL_FUNC/HAVE_$capfuncname/g" \
       -e "/GSL_FUNC_DOCSTRING/r docstring2.txt" \
       -e "/GSL_FUNC_DOCSTRING/d" \
       -e "${DEPREC_CMD}" \
       $1.template.cc

rm -f docstring2.txt

